
// Configuración de Google Cloud provider 
provider "google" {
  credentials =  file("./credentials/credentials.json")
  project = var.project
  region  = var.region
  version = "3.1"
}

